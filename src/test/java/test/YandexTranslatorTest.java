package test;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

class YandexTranslatorTest {
    @Test
    void shouldReturnTranslate() {
        given()
                .baseUri(    "https://translate.api.cloud.yandex.net/translate/v2/translate")
                .when()
                .get("/scheme/body.json")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
        ;
    }
}